# frozen_string_literal: true

require 'jwt'
require 'multi_json'
require 'net/http'

# Authentication middleware
#
# @see https://developers.cloudflare.com/access/setting-up-access/validate-jwt-tokens/
class CloudflareJwt
  # Custom decode token error.
  class DecodeTokenError < StandardError; end
  class TokenFormatError < StandardError; end

  # Certs path
  CERTS_PATH = '/cdn-cgi/access/certs'
  # Default algorithm
  DEFAULT_ALGORITHM = 'RS256'
  # CloudFlare JWT header.
  HEADER_NAME = 'HTTP_CF_ACCESS_JWT_ASSERTION'
  # Key for get current path.
  PATH_INFO = 'PATH_INFO'

  # Token regex.
  #
  # @see https://github.com/jwt/ruby-jwt/tree/v2.2.1#algorithms-and-usage
  TOKEN_REGEX = /
    ^(
    [a-zA-Z0-9\-_]+\.  # 1 or more chars followed by a single period
    [a-zA-Z0-9\-_]+\.  # 1 or more chars followed by a single period
    [a-zA-Z0-9\-_]+    # 1 or more chars, no trailing chars
    )$
  /x.freeze

  attr_reader :aud, :team_domain

  # Initializes middleware
  #
  # @example Initialize middleware in Rails
  #   config.middleware.use(
  #     Rack::CloudflareJwt::Auth,
  #     ENV['RACK_CLOUDFLARE_JWT_TEAM_DOMAIN'],
  #     <cloudflare-aud>
  #   )
  #
  # @param team_domain [String] the Team Domain (e.g. 'test.cloudflareaccess.com').
  # @param aud [String] the Cloudflare AUD.
  def initialize(team_domain, aud)
    @team_domain = team_domain
    @aud         = aud

    check_aud!
  end

  # Verify a token.
  def token_valid?(token)
    if missing_token?(token)
      raise_error('Authorization header')
    elsif invalid_token?(token)
      raise_error('Authorization header format')
    end

    decoded_token = public_keys.find do |key|
      break decode_token(token, key.public_key, aud)
    rescue DecodeTokenError => e
      logger.info e.message
      nil
    end

    if decoded_token
      return true
    else
      return false
    end
  end

  private

  # Private: Check aud.
  def check_aud!
    if !aud.is_a?(String) || aud.strip.empty?
      raise ArgumentError, 'policy AUD argument cannot be nil/empty'
    end
  end

  # Private: Decode a token.
  #
  # @param token [String] the token.
  # @param secret [String] the public key.
  # @param aud [String] the CloudFlare AUD.
  #
  # @example
  #
  #   [
  #     {"data"=>"test"}, # payload
  #     {"alg"=>"RS256"} # header
  #   ]
  #
  # @return [Array<Hash>] the token or `nil` at error.
  # @raise [DecodeTokenError] if the token is invalid.
  #
  # @see https://github.com/jwt/ruby-jwt/tree/v2.2.1#algorithms-and-usage
  def decode_token(token, secret, aud)
    ::JWT.decode(token, secret, true, aud: aud, verify_aud: true, algorithm: DEFAULT_ALGORITHM)
  rescue ::JWT::VerificationError
    raise DecodeTokenError, 'Invalid JWT token : Signature Verification Error'
  rescue ::JWT::ExpiredSignature
    raise DecodeTokenError, 'Invalid JWT token : Expired Signature (exp)'
  rescue ::JWT::IncorrectAlgorithm
    raise DecodeTokenError, 'Invalid JWT token : Incorrect Key Algorithm'
  rescue ::JWT::ImmatureSignature
    raise DecodeTokenError, 'Invalid JWT token : Immature Signature (nbf)'
  rescue ::JWT::InvalidIssuerError
    raise DecodeTokenError, 'Invalid JWT token : Invalid Issuer (iss)'
  rescue ::JWT::InvalidIatError
    raise DecodeTokenError, 'Invalid JWT token : Invalid Issued At (iat)'
  rescue ::JWT::InvalidAudError
    raise DecodeTokenError, 'Invalid JWT token : Invalid Audience (aud)'
  rescue ::JWT::InvalidSubError
    raise DecodeTokenError, 'Invalid JWT token : Invalid Subject (sub)'
  rescue ::JWT::InvalidJtiError
    raise DecodeTokenError, 'Invalid JWT token : Invalid JWT ID (jti)'
  rescue ::JWT::DecodeError
    raise DecodeTokenError, 'Invalid JWT token : Decode Error'
  end

  # Private: Check if auth header is invalid.
  #
  # @return [Boolean] true if it is, false otherwise.
  def invalid_token?(token)
    token !~ TOKEN_REGEX
  end

  # Private: Check if no auth header.
  #
  # @return [Boolean] true if it is, false otherwise.
  def missing_token?(token)
    token.nil? || token.strip.empty?
  end

  # Private: Return an error.
  def raise_error(message)
    raise TokenFormatError, message
  end

  # Private: Get public keys.
  #
  # @return [Array<OpenSSL::PKey::RSA>] the public keys.
  def public_keys
    fetch_public_keys_cached.map do |jwk_data|
      ::JWT::JWK.import(jwk_data).keypair
    end
  end

  # Private: Fetch public keys.
  #
  # @return [Array<Hash>] the public keys.
  def fetch_public_keys
    json = Net::HTTP.get(team_domain, CERTS_PATH)
    json.empty? ? [] : MultiJson.load(json, symbolize_keys: true).fetch(:keys)
  rescue StandardError
    []
  end

  # Private: Get cached public keys.
  #
  # Store a keys in the cache only 10 minutes.
  #
  # @return [Array<Hash>] the public keys.
  def fetch_public_keys_cached
    key = [self.class.name, '#secrets'].join('_')

    if defined? Rails
      Rails.cache.fetch(key, expires_in: 600) { fetch_public_keys }
    elsif defined? Padrino
      keys = Padrino.cache[key]
      keys || Padrino.cache.store(key, fetch_public_keys, expires: 600)
    else
      fetch_public_keys
    end
  end

  # Private: Get a logger.
  #
  # @return [ActiveSupport::Logger] the logger.
  def logger
    if defined? Rails
      Rails.logger
    elsif defined? Padrino
      Padrino.logger
    end
  end
end
