module CloudflareAuth
  extend ActiveSupport::Concern

  CLOUDFLARE_AUTH_APP_DOMAIN = 'auth.alexpopp.com'
  CLOUDFLARE_AUTH_TEAM_DOMAIN = 'popp.cloudflareaccess.com'
  CLOUDFLARE_AUTH_AUD = '88901dd76184d51359957457b13d777205a40d08ee4c08c6129ad10670b7a7fe'

  AUTH_PARAM_NAME = :bukephalos_auth
  AUTH_COOKIE_NAME = AUTH_PARAM_NAME

  def authenticate_cloudflare_jwt
    param_was_written_to_cookie = set_auth_cookie_if_passed_via_param

    token = cookies[AUTH_COOKIE_NAME]
    jwt = CloudflareJwt.new(CLOUDFLARE_AUTH_TEAM_DOMAIN, CLOUDFLARE_AUTH_AUD)
    token_valid = begin
      jwt.token_valid?(token)
    rescue CloudflareJwt::TokenFormatError => e
      Rails.logger.debug e.message
      false
    end

    if !token_valid
      expires_now
      redirect_params = { redirect: request.original_url }
      redirect_url = remove_param_from_url("https://#{CLOUDFLARE_AUTH_APP_DOMAIN}?#{redirect_params.to_query}", AUTH_PARAM_NAME)
      redirect_to redirect_url, allow_other_host: true
      return
    end

    if param_was_written_to_cookie
      redirect_to remove_param_from_url(request.original_url, AUTH_PARAM_NAME)
      return
    end
  end

  private

  def set_auth_cookie_if_passed_via_param
    token = params[AUTH_PARAM_NAME]
    if token
      cookies[AUTH_COOKIE_NAME] = {
        value: token,
        expires: 1.week,
        domain: :all,
        httponly: true
      }

      return true
    end

    false
  end

  def remove_param_from_url(url, param)
    uri = URI.parse(url)
    params = URI.decode_www_form(uri.query || '').reject { |k, _| k == param.to_s }
    uri.query = URI.encode_www_form(params)
    uri.to_s.chomp('?')
  end

end
