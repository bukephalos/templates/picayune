require 'arbre'

class ApplicationController < ActionController::Base
  include CloudflareAuth

  before_action :authenticate_cloudflare_jwt

  layout 'application'

end