let closeButtons = document.querySelectorAll('.flash-message button');
closeButtons.forEach(button => {
  button.onclick = function(e) {
    let currentButton = e.currentTarget;
    let parent = currentButton.parentElement;
    while (!parent.classList.contains('flash-message'))
      parent = parent.parentElement;
    parent.remove();
  }
});
