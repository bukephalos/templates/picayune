$:.push File.expand_path('lib', __dir__)

require 'picayune/version'

Gem::Specification.new do |spec|
  spec.name        = 'picayune'
  spec.version     = Picayune::VERSION
  spec.authors     = ['alxppp']
  spec.email       = ['alxppp@gmail.com']
  spec.summary     = 'Minimalistic Rails setup'
  spec.description = 'Minimalistic Rails with Cloudflare JWT auth and Tailwind CSS'
  spec.license     = 'MIT'

  spec.files = Dir['{app,lib,public}/**/*']

  spec.add_dependency 'rails', '~> 7'
  spec.add_runtime_dependency 'jwt', '~> 2'
  spec.add_runtime_dependency 'multi_json', '~> 1'
  spec.add_runtime_dependency 'arbre', '~> 2'
end
